# Some Thoughts on Datse Mark Up

I have worked with stuff like Markdown, Restructured Text, and Twee (the 
markup language of Twine) and find that I keep forgetting which uses
which format for the various things which we use.

The code we are using, is using Markdown text, but I'm not really sure
what version of that, as it's on GitLab, and I want to have that work.

So, I decided I wanted to have a format that I would be able to work 
with which I don't need to adapt for different formats.

Of course, that idea is really not a very smart one to be honest as it is
something that is, "making a new standard".

As I'm writing this, I think a future "tool" would be to convert to and 
from .damu to different formats.

## Some principles

- Keep it simple
- Keep it human readable
- Rendering should be to maintain ease of access

### Additional comments about that

- Multiple pages might lead to issues which would have updates around 
  broken and working links.
- Offsite links would not be checked, but could be maintained in the 
  markup to handle indication of whether a link is broken or working.

## Requirements within the language

There are a few things that will be required to be able to be handled with
the language, that I want to make a note of.

### Minimual Requirements

- Paragraph Text
- Links
  - Link Text
- Lists
  - Unordered Lists
  - Ordered Lists
  - List nesting

### Desired Elements

- Styling (probably outside of markup)
- Tables
- Definition Lists
- Images
