# Some thoughts about licensing

We have selected a GPLv3 license, though we are not set on that, and 
future developers are welcome to look at other licenses.  

This isn't that people would be able to go "against" the GPL but more
that we would not want to restrict the code to the GPL.

If a previous developer is not avaialble to give an answer as to a 
change in the licensing that we are considering, the development
team (provided they haven't been recently active, and contact attempts
have failed over a reasonable period of time) may consider that their code
may be either split off, or relisenced.  
